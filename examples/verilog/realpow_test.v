/**********************************************************************
 * $realpow example -- Verilog HDL test bench.
 *
 * Verilog test bench to test the $realpow PLI application.
 *
 * For the book, "The Verilog PLI Handbook" by Stuart Sutherland
 *  Book copyright 1999, Kluwer Academic Publishers, Norwell, MA, USA
 *   Contact: www.wkap.il
 *  Example copyright 1998, Sutherland HDL Inc, Portland, Oregon, USA
 *   Contact: www.sutherland.com or (503) 692-0898
 *********************************************************************/
`timescale 1ns / 1ns
module test;
  integer i;
  real    m, n;

  initial
    begin
      i = 0;
      m = 1.5;
      n = 0.5;
      #1 $display("$realpow(1.5, 3) returns %2.2f",
                   $realpow(1.5, 3));
      #1 $display("$realpow(4.0, n) returns %2.2f (n=%2.2f)",
                   $realpow(4.0, n), n);
      #1 $display("$realpow(m, i)   returns %2.2f (m=%2.2f i=%0d)",
                   $realpow(m, i), m, i);
    end

endmodule

/*********************************************************************/
