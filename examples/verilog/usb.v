// Generated by vstruct v2.2.1

`include "usb_defines.v"

module usb (
    // WISHBONE Interface
    clk_i,
    addr_i,
    data_i,
    data_o,
    ack_o,
    we_i,
    stb_i,
    cyc_i,
    inta_o,
    intb_o,
    dma_req,
    dma_ack,
    rst_i,
    resume_req_i,

    // UTMI Interface
    phy_clk,
    DataOut,
    TxReady,
    RxValid,
    RxActive,
    RxError,
    DataIn,
    XcvSelect,
    TermSel,
    SuspendM,
    LineState,
    OpMode,
    usb_vbus,
    VStatus,                           // UTMI Vendor C/S bus
    VControl,
    VControl_Load,
    TxValid,
    phy_rst
    );

    // WISHBONE Interface
    input           clk_i;
    input    [17:0] addr_i;
    input    [31:0] data_i;
    output   [31:0] data_o;
    output          ack_o;
    input           we_i;
    input           stb_i;
    input           cyc_i;
    output          inta_o;
    output          intb_o;
    output   [14:0] dma_req;
    input    [14:0] dma_ack;
    input           rst_i;
    input           resume_req_i;

    // UTMI Interface
    input           phy_clk;
    output   [7:0]  DataOut;
    input           TxReady;
    input           RxValid;
    input           RxActive;
    input           RxError;
    input    [7:0]  DataIn;
    output          XcvSelect;
    output          TermSel;
    output          SuspendM;
    input    [1:0]  LineState;
    output   [1:0]  OpMode;
    input           usb_vbus;
    input    [7:0]  VStatus;           // UTMI Vendor C/S bus
    output   [3:0]  VControl;
    output          VControl_Load;
    output          TxValid;
    output          phy_rst;

    ////////////////////////////////////////////////////////////////////////////
    // wishbone_if                                                            //
    ////////////////////////////////////////////////////////////////////////////
    wire     [14:0] wadr;
    wire     [31:0] wb2ma_d;
    wire            wwe;
    wire            wreq;
    wire            re;
    wire            we;
    wire     [31:0] wb2rf_d;

    ////////////////////////////////////////////////////////////////////////////
    // usb_trans_mac_if                                                       //
    ////////////////////////////////////////////////////////////////////////////
    wire            rst;
    wire            tx_valid_out;
    wire     [7:0]  rx_data;
    wire            rx_valid;
    wire            rx_active;
    wire            rx_err;
    wire            tx_ready;
    wire            mode_hs;           // High Speed Mode
    wire            usb_reset;         // USB Reset
    wire            usb_suspend;       // USB Suspend
    wire            usb_attached;      // Attached to USB
    wire            resume_req;

    ////////////////////////////////////////////////////////////////////////////
    // protocol_layer                                                         //
    ////////////////////////////////////////////////////////////////////////////
    wire     [7:0]  tx_data;
    wire            tx_valid;
    wire            tx_valid_last;
    wire            tx_first;
    wire     [14:0] madr;              // word address
    wire     [31:0] mdout;
    wire            mwe;
    wire            mreq;
    wire     [31:0] idin;              // Data Input
    wire     [3:0]  ep_sel;            // Endpoint Number Input
    wire            buf0_rl;           // Reload Buf 0 with original values
    wire            buf0_set;          // Write to buf 0
    wire            buf1_set;          // Write to buf 1
    wire            uc_bsel_set;       // Write to the uc_bsel field
    wire            uc_dpd_set;        // Write to the uc_dpd field
    wire            int_buf1_set;      // Set buf1 full/empty interrupt
    wire            int_buf0_set;      // Set buf0 full/empty interrupt
    wire            int_upid_set;      // Set unsupported PID interrupt
    wire            int_crc16_set;     // Set CRC16 error interrupt
    wire            int_to_set;        // Set time out interrupt
    wire            int_seqerr_set;    // Set PID sequence error interrupt
    wire     [31:0] frm_nat;
    wire            pid_cs_err;        // pid checksum error
    wire            nse_err;           // no such endpoint error
    wire            crc5_err;          // crc5 error

    ////////////////////////////////////////////////////////////////////////////
    // memory_arbiter                                                         //
    ////////////////////////////////////////////////////////////////////////////
    wire     [14:0] radr;
    wire     [31:0] arb2mem;
    wire            rre;
    wire            rwe;
    wire     [31:0] mdin;
    wire            mack;
    wire     [31:0] ma2wb_d;
    wire            wack;

    ////////////////////////////////////////////////////////////////////////////
    // buff_mem                                                               //
    ////////////////////////////////////////////////////////////////////////////
    wire     [31:0] mem2arb;

    ////////////////////////////////////////////////////////////////////////////
    // reg_file                                                               //
    ////////////////////////////////////////////////////////////////////////////
    wire     [31:0] rf2wb_d;
    wire            match;             // Endpoint Matched
    wire     [31:0] csr;               // Internal CSR Output
    wire     [31:0] buf0;              // Internal Buf 0 Output
    wire     [31:0] buf1;              // Internal Buf 1 Output
    wire     [6:0]  funct_adr;         // Function Address
    wire            dma_in_buf_sz1;
    wire            dma_out_buf_avail;
    wire            rf_resume_req;

    ///////////////////////////////////////////////////////////////////
    //
    // Misc Logic
    //
    assign TxValid = tx_valid_out;
    assign rst = rst_i;
    assign phy_rst = ~rst;
    assign resume_req = resume_req_i | rf_resume_req;

    wb wishbone_if (
            // WISHBONE Interface
            .clk               (clk_i),
            .rst               (rst),
            .addr_i            (addr_i),
            .data_i            (data_i),
            .data_o            (data_o),
            .ack_o             (ack_o),
            .we_i              (we_i),
            .stb_i             (stb_i),
            .cyc_i             (cyc_i),

            // UTMI Interface
            .phy_clk           (phy_clk),

            // Memory Arbiter Interface
            .wadr              (wadr),
            .wdout             (wb2ma_d),
            .wdin              (ma2wb_d),
            .wwe               (wwe),
            .wreq              (wreq),
            .wack              (wack),

            // Register File interface
            .re                (re),
            .we                (we),
            .din               (rf2wb_d),
            .dout              (wb2rf_d)
        ); // wishbone_if

    utmi_if usb_trans_mac_if (
            // UTMI Interface (EXTERNAL)
            .phy_clk           (phy_clk),
            .rst               (rst),
            .DataOut           (DataOut),
            .TxValid           (tx_valid_out),
            .TxReady           (TxReady),
            .RxValid           (RxValid),
            .RxActive          (RxActive),
            .RxError           (RxError),
            .DataIn            (DataIn),
            .XcvSelect         (XcvSelect),
            .TermSel           (TermSel),
            .SuspendM          (SuspendM),
            .LineState         (LineState),
            .OpMode            (OpMode),
            .usb_vbus          (usb_vbus),

            // Internal Interface
            .rx_data           (rx_data),
            .rx_valid          (rx_valid),
            .rx_active         (rx_active),
            .rx_err            (rx_err),
            .tx_data           (tx_data),
            .tx_valid          (tx_valid),
            .tx_valid_last     (tx_valid_last),
            .tx_ready          (tx_ready),
            .tx_first          (tx_first),

            // Misc Interfaces
            .mode_hs           (mode_hs),           // High Speed Mode
            .usb_reset         (usb_reset),         // USB Reset
            .usb_suspend       (usb_suspend),       // USB Suspend
            .usb_attached      (usb_attached),      // Attached to USB
            .resume_req        (resume_req)
        ); // usb_trans_mac_if

    pl protocol_layer (
            // UTMI Interface
            .clk               (phy_clk),
            .rst               (rst),

            // UTMI Interface
            .rx_data           (rx_data),
            .rx_valid          (rx_valid),
            .rx_active         (rx_active),
            .rx_err            (rx_err),
            .tx_data           (tx_data),
            .tx_valid          (tx_valid),
            .tx_valid_last     (tx_valid_last),
            .tx_ready          (tx_ready),
            .tx_first          (tx_first),
            .tx_valid_out      (tx_valid_out),
            .mode_hs           (mode_hs),           // High Speed Mode
            .usb_reset         (usb_reset),         // USB Reset
            .usb_suspend       (usb_suspend),       // USB Suspend
            .usb_attached      (usb_attached),      // Attached to USB

            // memory interface
            .madr              (madr),              // word address
            .mdout             (mdout),
            .mdin              (mdin),
            .mwe               (mwe),
            .mreq              (mreq),
            .mack              (mack),

            // Register File Interface
            .fa                (funct_adr),         // Function Address (as set by the controller)
            .idin              (idin),              // Data Input
            .ep_sel            (ep_sel),            // Endpoint Number Input
            .match             (match),             // Endpoint Matched
            .dma_in_buf_sz1    (dma_in_buf_sz1),
            .dma_out_buf_avail (dma_out_buf_avail),

            .buf0_rl           (buf0_rl),           // Reload Buf 0 with original values
            .buf0_set          (buf0_set),          // Write to buf 0
            .buf1_set          (buf1_set),          // Write to buf 1
            .uc_bsel_set       (uc_bsel_set),       // Write to the uc_bsel field
            .uc_dpd_set        (uc_dpd_set),        // Write to the uc_dpd field

            .int_buf1_set      (int_buf1_set),      // Set buf1 full/empty interrupt
            .int_buf0_set      (int_buf0_set),      // Set buf0 full/empty interrupt
            .int_upid_set      (int_upid_set),      // Set unsupported PID interrupt
            .int_crc16_set     (int_crc16_set),     // Set CRC16 error interrupt
            .int_to_set        (int_to_set),        // Set time out interrupt
            .int_seqerr_set    (int_seqerr_set),    // Set PID sequence error interrupt

            .csr               (csr),               // Internal CSR Output
            .buf0              (buf0),              // Internal Buf 0 Output
            .buf1              (buf1),              // Internal Buf 1 Output

            // Misc
            .frm_nat           (frm_nat),

            // Misc
            .pid_cs_err        (pid_cs_err),        // pid checksum error
            .nse_err           (nse_err),           // no such endpoint error
            .crc5_err          (crc5_err)           // crc5 error

        ); // protocol_layer

    mem_arb memory_arbiter (
            .phy_clk           (phy_clk),
            .wclk              (clk_i),
            .rst               (rst),

            // SSRAM Interface
            .radr              (radr),
            .rdin              (mem2arb),
            .rdout             (arb2mem),
            .rre               (rre),
            .rwe               (rwe),

            // IDMA Memory Interface
            .madr              (madr),
            .mdout             (mdin),
            .mdin              (mdout),
            .mwe               (mwe),
            .mreq              (mreq),
            .mack              (mack),

            // WISHBONE Memory Interface
            .wadr              (wadr),
            .wdout             (ma2wb_d),
            .wdin              (wb2ma_d),
            .wwe               (wwe),
            .wreq              (wreq),
            .wack              (wack)

        ); // memory_arbiter

    ssram buff_mem (
            .clk               (phy_clk),
            .rst               (rst),
            .radr              (radr),
            .rdin              (arb2mem),
            .rdout             (mem2arb),
            .rre               (rre),
            .rwe               (rwe)
        ); // buff_mem

    rf reg_file (
            .clk               (phy_clk),
            .wclk              (clk_i),
            .rst               (rst),

            // Wishbone Interface
            .adr               (wadr[6:0]),
            .re                (re),
            .we                (we),
            .din               (wb2rf_d),
            .dout              (rf2wb_d),
            .inta              (inta_o),
            .intb              (intb_o),
            .dma_req           (dma_req),
            .dma_ack           (dma_ack),

            // Internal Interface
            .idin              (idin),              // Data Input
            .ep_sel            (ep_sel),            // Endpoint Number Input
            .match             (match),             // Endpoint Matched
            .buf0_rl           (buf0_rl),           // Reload Buf 0 with original values

            .buf0_set          (buf0_set),          // Write to buf 0
            .buf1_set          (buf1_set),          // Write to buf 1
            .uc_bsel_set       (uc_bsel_set),       // Write to the uc_bsel field
            .uc_dpd_set        (uc_dpd_set),        // Write to the uc_dpd field

            .int_buf1_set      (int_buf1_set),      // Set buf1 full/empty interrupt
            .int_buf0_set      (int_buf0_set),      // Set buf0 full/empty interrupt
            .int_upid_set      (int_upid_set),      // Set unsupported PID interrupt
            .int_crc16_set     (int_crc16_set),     // Set CRC16 error interrupt
            .int_to_set        (int_to_set),        // Set time out interrupt
            .int_seqerr_set    (int_seqerr_set),    // Set PID Sequence Error Interrupt

            .csr               (csr),               // Internal CSR Output
            .buf0              (buf0),              // Internal Buf 0 Output
            .buf1              (buf1),              // Internal Buf 1 Output
            .funct_adr         (funct_adr),         // Function Address
            .dma_in_buf_sz1    (dma_in_buf_sz1),
            .dma_out_buf_avail (dma_out_buf_avail),

            // Misc
            .frm_nat           (frm_nat),

            // UTMI Interface
            .utmi_vend_stat    (VStatus),           // UTMI Vendor C/S bus
            .utmi_vend_ctrl    (VControl),
            .utmi_vend_wr      (VControl_Load),

            .line_stat         (LineState),         // Below are signals for interrupt generation
            .usb_attached      (usb_attached),
            .mode_hs           (mode_hs),
            .suspend           (usb_suspend),
            .attached          (usb_attached),
            .usb_reset         (usb_reset),
            .pid_cs_err        (pid_cs_err),
            .nse_err           (nse_err),
            .crc5_err          (crc5_err),
            .rx_err            (rx_err),
            .rf_resume_req     (rf_resume_req)
        ); // reg_file
endmodule

