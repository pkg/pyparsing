
module test ( a,b,d);
	input [4:0] a;
	input [20:0] b;
	output d;

   always @(a or b)
   begin
      if (a == 5'b00000)
      begin
         d = b[2:0] ; 
      end
      else if (a == 5'b00001)
      begin
         d = b[5:3] ; 
      end
      else if (a == 5'b00010)
      begin
         d = b[8:6] ; 
      end
      else if (a == 5'b00011)
      begin
         d = b[11:9] ; 
      end
      else if (a == 5'b00100)
      begin
         d = b[14:12] ; 
      end
      else if (a == 5'b00101)
      begin
         d = b[17:15] ; 
      end
      else if (a == 5'b00110)
      begin
         d = b[20:18] ; 
      end
      else if (a == 5'b00111)
      begin
         d = b[2:0] ; 
      end
      else if (a == 5'b01000)
      begin
         d = b[5:3] ; 
      end
      else if (a == 5'b01001)
      begin
         d = b[8:6] ; 
      end
      else if (a == 5'b01010)
      begin
         d = b[11:9] ; 
      end
      else if (a == 5'b01011)
      begin
         d = b[14:12] ; 
      end
      else if (a == 5'b01100)
      begin
         d = b[17:15] ; 
      end
      else if (a == 5'b01101)
      begin
         d = b[20:18] ; 
      end
      else if (a == 5'b01110)
      begin
         d = b[2:0] ; 
      end
      else if (a == 5'b01111)
      begin
         d = b[5:3] ; 
      end
      else if (a == 5'b10000)
      begin
         d = b[8:6] ; 
      end
      else if (a == 5'b10001)
      begin
         d = b[11:9] ; 
      end
      else if (a == 5'b10010)
      begin
         d = b[14:12] ; 
      end
      else if (a == 5'b10011)
      begin
         d = b[17:15] ; 
      end
      else if (a == 5'b10100)
      begin
         d = b[20:18] ; 
      end
      else if (a == 5'b10101)
      begin
         d = b[2:0] ; 
      end
      else if (a == 5'b10110)
      begin
         d = b[5:3] ; 
      end
      else if (a == 5'b10111)
      begin
         d = b[8:6] ; 
      end
      else if (a == 5'b11000)
      begin
         d = b[11:9] ; 
      end
      else if (a == 5'b11001)
      begin
         d = b[14:12] ; 
      end
      else if (a == 5'b11010)
      begin
         d = b[17:15] ; 
      end
      else if (a == 5'b11011)
      begin
         d = b[20:18] ; 
      end
      else if (a == 5'b11111)
      begin
         d = 3'b0; 
      end
      else
      begin
         d = {3{1'b0}} ; 
      end 
   end 
endmodule
